const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

const UsuarioSchema = new mongoose.Schema({
    nombre: {
        type: String,
        required: true,
        trim: true,
        minlength: 3
    },
    foto: {
        type: String
    },
    cartera: {
        type: Number,
        default: 100
    },
    user: {
        type: String,
        required: true,
        trim: true,
        minlength: 3,
        lowercase: true,
        unique: 'El usuario {VALUE} ya ha sido registrado'
    },
    pass: {
        type: String,
        require: true,
        minlength: 6
    },
    role: {
        type: String,
        default: 'Cliente'
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    creado: {
        type: Date,
        default: Date.now
    }
});

UsuarioSchema.methods.toJSON = function () {
    const Usuario = this;
    const usuario = Usuario.toObject();

    return _.difference(usuario, ['pass', 'tokens']);
};

UsuarioSchema.methods.generateAuthToken = function () {
    const Usuario = this;
    const token = jwt.sign({ _id: Usuario._id.toHexString() }, process.env.JWT_SECRET).toString();

    Usuario.tokens.push({ token });

    return Usuario.save().then(() => {
        return token;
    });
};

UsuarioSchema.methods.removeToken = function (token) {
    const Usuario = this;

    return Usuario.update({
        $pull: {
            tokens: { token }
        }
    });
};

UsuarioSchema.statics.findByToken = function (token) {
    const Usuario = this;
    let decoded;

    try {
        decoded = jwt.verify(token, process.env.JWT_SECRET);
    } catch (e) {
        return Promise.reject(e);
    }

    return Usuario.findOne({
        '_id': decoded._id,
        'tokens.token': token
    });
};

UsuarioSchema.statics.findByCredentials = function (user, pass) {
    const Usuario = this;

    return Usuario.findOne({
        user: user.toLowerCase()
    }).then((usuario) => {
        if (!usuario) {
            return Promise.reject({ mensaje: 'Usuario no registrado' });
        }

        return new Promise((resolve, reject) => {
            bcrypt.compare(pass, usuario.pass, (err, res) => {
                if (res) {
                    resolve(usuario);
                } else {
                    reject({ mensaje: 'Contraseña incorrecta' });
                }
            });
        });
    });
};

UsuarioSchema.methods.verifyCredentials = function (pass) {
    const usuario = this;
    return bcrypt.compare(pass, usuario.pass);
};

UsuarioSchema.pre('save', function (next) {
    const Usuario = this;

    if (Usuario.isModified('pass')) {
        bcrypt.genSalt(10, (err, salt) => {
            bcrypt.hash(Usuario.pass, salt, (err, hash) => {
                Usuario.pass = hash;
                next();
            });
        });
    } else {
        next();
    }
});

const Usuario = mongoose.model('Usuario', UsuarioSchema);

module.exports = { Usuario }
