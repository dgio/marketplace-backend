require('./config/config');
require('./db/mongoose');

const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');

const { authenticate } = require('./middleware/authenticate');

const { ObjectID } = require('mongodb');
const { Usuario } = require('./models/usuario');
const { Producto } = require('./models/producto');
const { Historial } = require('./models/historial');

const port = process.env.PORT;

const app = express();

app.use(bodyParser.json());

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "HEAD, OPTIONS, GET, POST, PUT, DELETE, PATCH");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
  res.header("Access-Control-Expose-Headers", "Content-Type, Authorization")
  next();
});

/*** Usuarios ***/
// Nuevo usuario
app.post('/usuarios', async (req, res) => {
  try {
    const usuario = new Usuario(req.body);
    await usuario.save();
    const token = await usuario.generateAuthToken();
    const u = await _.omit(usuario.toObject(), ['pass', 'tokens']);
    res.header('Authorization', token).send(u);
  } catch (e) {
    res.status(400).send(e);
  }
});

// Información de perfil
app.get('/usuarios/yo', authenticate, async (req, res) => {
  try {
    const usuario = await _.omit(req.usuario.toObject(), ['pass', 'tokens']);
    res.send(usuario);
  } catch (e) {
    res.status(400).send(e);
  }
});

// Actualizar usuario
app.put('/usuarios/yo', authenticate, async (req, res) => {
  try {
    const usuario = await Usuario.findByIdAndUpdate(req.usuario._id, req.body, { new: true });
    usuario ? res.send(usuario.toObject()) : res.status(404).send();
  } catch (e) {
    res.status(400).send(e);
  }
});

// Recarga para cartera de usuario
app.put('/recarga', authenticate, async (req, res) => {
  try {
    const usuario = await Usuario.findByIdAndUpdate(req.usuario._id, { $inc: { cartera: req.body.cantidad } }, { new: true });
    usuario ? res.send(usuario.toObject()) : res.status(404).send();
  } catch (e) {
    res.status(400).send(e);
  }
});

// Iniciar sesión
app.post('/login', async (req, res) => {
  try {
    const usuario = await Usuario.findByCredentials(req.body.user, req.body.pass);
    const token = await usuario.generateAuthToken();
    res.header('Authorization', token).send(usuario.toObject());
  } catch (e) {
    res.status(400).send(e);
  }
});

// Cerrar sesión
app.delete('/logout', authenticate, async (req, res) => {
  try {
    await req.usuario.removeToken(req.token);
    res.status(200).send();
  } catch (e) {
    res.status(400).send();
  }
});

/*** Productos ***/
// Nuevo producto
app.post('/productos', authenticate, async (req, res) => {
  try {
    if (req.usuario.role !== 'Admin') return res.status(401).send();
    const producto = new Producto(req.body);
    await producto.save();
    producto ? res.send(producto) : res.status(404).send();
  } catch (e) {
    res.status(400).send(e);
  }
});

// Lista de productos
app.get('/productos', authenticate, async (req, res) => {
  try {
    const productos = await Producto.find();
    productos ? res.send(productos) : res.status(404).send();
  } catch (e) {
    res.status(400).send(e);
  }
});

// Detalle de producto
app.get('/productos/:id', authenticate, async (req, res) => {
  try {
    let id = req.params.id;
    if (!ObjectID.isValid(id)) return res.status(400).send();
    const producto = await Producto.findOne({ _id: id });
    producto ? res.send(producto) : res.status(404).send();
  } catch (e) {
    res.status(400).send(e);
  }
});

// Actualizar producto
app.put('/productos/:id', authenticate, async (req, res) => {
  try {
    if (req.usuario.role !== 'Admin') return res.status(401).send();
    const id = req.params.id;
    if (!ObjectID.isValid(id)) return res.status(400).send();
    const producto = await Producto.findByIdAndUpdate(id, req.body, { new: true });
    producto ? res.send(producto.toObject()) : res.status(404).send();
  } catch (e) {
    res.status(400).send(e);
  }
});

// Modificar inventario
app.put('/productos/:id/inventario', authenticate, async (req, res) => {
  try {
    if (req.usuario.role !== 'Admin') return res.status(401).send();
    const id = req.params.id;
    if (!ObjectID.isValid(id)) return res.status(400).send();
    const cantidad = req.body.cantidad;
    if (!Number(cantidad)) return res.status(400).send();
    const producto = await Producto.findByIdAndUpdate(id, { $inc: { cantidad } }, { new: true });
    producto ? res.send(producto.toObject()) : res.status(404).send();
  } catch (e) {
    res.status(400).send(e);
  }
});

/*** Historial ***/
// Hacer una venta 
/* 
1.- Verfica cartera de usuario 
2.- Reduce la cantidad de inventario de los productos
4.- Actualzia cartera de usuario
3.- Agrega la información al historial de ventas.
*/
app.post('/ventas', authenticate, async (req, res) => {
  try {
    const cartera = req.usuario.cartera;
    const body = req.body;
    const _productos = _.map(body, '_id');
    const productos = await Producto.find({
      _id: {
        $in: _productos
      }
    });

    let venta = [];

    _.forEach(productos, p => {
      _.forEach(body, b => {
        if (String(p._id) === b._id) {
          p.cantidad = b.cantidad;
          venta.push(p);
        }
      });
    });

    const total = _.sumBy(venta, (v) => {
      return v.precio * v.cantidad;
    });

    if (cartera < total) return res.status(402).send({ ok: false, mensaje: 'Cantidad insuficiente para completar la transacción' });

    _.forEach(venta, v => {
      Producto.findByIdAndUpdate(v._id, { $inc: { cantidad: -v.cantidad } }, { new: true }).catch(err => {
        return res.status(500).send({ ok: false, mensaje: 'Error al actualizar el inventario' });
      });
    });

    const usuario = await Usuario.findByIdAndUpdate(req.usuario._id, { $inc: { cartera: -total } }, { new: true });
    if (!usuario) return res.status(500).send({ ok: false, mensaje: 'Error al procesar el pago' });

    const hist = {
      _usuario: usuario._id,
      user: usuario.user,
      nombre: usuario.nombre,
      total,
      detalle: venta
    }

    const historial = new Historial(hist);
    await historial.save();
    historial ? res.send(historial) : res.status(404).send();
    res.send();
  } catch (e) {
    res.status(400).send(e);
  }
});

// Historial de compras general
app.get('/historial', authenticate, async (req, res) => {
  try {
    if (req.usuario.role !== 'Admin') return res.status(401).send();
    const historial = await Historial.find();
    historial ? res.send(historial) : res.status(404).send();
  } catch (e) {
    res.status(400).send(e);
  }
});

// Historial de compras de cliente
app.get('/historial/yo', authenticate, async (req, res) => {
  try {
    const historial = await Historial.find({_usuario: req.usuario._id});
    historial ? res.send(historial) : res.status(404).send();
  } catch (e) {
    res.status(400).send(e);
  }
});

// Detalle de historial
app.get('/historial/:id', authenticate, async (req, res) => {
  try {
    let id = req.params.id;
    if (!ObjectID.isValid(id)) return res.status(400).send();
    const historial = await Historial.findOne({ _id: id });
    historial ? res.send(historial) : res.status(404).send();
  } catch (e) {
    res.status(400).send(e);
  }
});

app.listen(port, () => {
  console.log(`Servidor iniciado en el puerto ${port}`);
});

module.exports = { app };